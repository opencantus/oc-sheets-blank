@echo off
cls
echo Converting pdf files to 300 dpi png images in all sub-folders...
for /D %%d in (*) do (
	echo Processing sub-folder '%%d'
	cd %%d
	for %%f in (*.pdf) do (
		echo %%f
		gm convert -density 300 %%f +adjoin %%~nf-%%02d.png
	)
	cd ..
)