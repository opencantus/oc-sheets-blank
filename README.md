OpenCantus.org - PDF- und PNG-Ausgabe
=====================================

Für nicht gewerbliche und nicht öffentliche Nutzung bestimmt.
Sofern nicht anders angegeben, stehen alle urheberrechtlich schützenswerten
Inhalte unter der Creative-Commons-Lizenz Namensnennung - Weitergabe unter
gleichen Bedingungen 4.0 International
(https://creativecommons.org/licenses/by-sa/4.0/deed.de).

---

All files in this repository are part of the OpenCantus project.
The OpenCantus project is free software;
you can redistribute it and/or modify it under the terms of
Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
http://creativecommons.org/licenses/by-sa/4.0/
