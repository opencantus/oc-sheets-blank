@echo off
if "%1"=="" goto blank

cls
echo Processing tex files in sub-folder %1...
cd %1
for %%f in (*.tex) do (
	echo %%f
	xelatex -synctex=-1 -max-print-line=120 -interaction=nonstopmode "%%f" > tex.log
	rm tex.log
)
cd ..
echo done.
goto done

:blank
echo Please provide a sub-folder to process.
echo e.g. tex2pdf F
goto done

:done