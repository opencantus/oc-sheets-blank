@echo off
if "%1"=="" goto blank

echo "%~d1%~p1%~n1-%%02d.png"
gm convert -density 300 "%~d1%~p1%~n1%~x1" +adjoin "%~d1%~p1%~n1-%%02d.png"
for %%f in ("%~d1%~p1%~n1-00.png") do @echo %%~zf bytes written. Done.
goto done

:blank
echo Please provide a file to convert.
echo e.g. pdf2png C\ca_ca_geschmauset_oc.pdf
goto done

:done