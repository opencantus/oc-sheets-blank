@echo off
cls
echo Removing unused files from all sub-folders:
for /D %%d in (*) do (
	echo|set /p= '%%d' 
	cd %%d
	for %%x in (*.aux *.bbl *.blg *.log *.synctex) do del %%x
	cd ..
)
echo done.
